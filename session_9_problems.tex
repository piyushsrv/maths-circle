\documentclass[letterpaper,11pt,reqno]{article}
\input{macros}
\newtoggle{notest}
\toggletrue{notest}

\iftoggle{notest}{%
    \NewEnviron{notes}{\color{blue} \BODY} %
  }{%
    \NewEnviron{notes}{}
  }
\usetikzlibrary{calc}
\usepackage{gensymb,tfrupee}
\usepackage[yyyymmdd,hhmmss]{datetime}
\usepackage{float} % for enforcing figure placement
%\usepackage{soul}

\newcommand{\oldfactorial}[1]{%
\tikz[baseline]{\node[anchor=base,inner sep=0.3ex](mynode){\ensuremath{#1}};\draw(mynode.north west)--(mynode.south west)--(mynode.south east);\path[use as bounding box]($(mynode.south west)+(-0.3ex,-0.3ex)$)rectangle($(mynode.north east)+(0.3ex,0.3ex)$);}
}

\newcommand{\peak}[1]{\ensuremath{\mathrm{peak}\inp{#1}}}
\newcommand{\conf}[1]{\ensuremath{\mathrm{ConflictSet}\inp{#1}}}

\title{Maths Circle India}
\author{TIFR-STCS Maths Circle Team}
\date{Session 9: September 8, 2023}%\\Compiled at \currenttime, \today}

\begin{document}
\maketitle


\begin{notes}
  \paragraph{General notes}
  \begin{enumerate}
  \item Some of the students are much faster than the others.  So, to give
    everyone a fair chance, it might help to sometime call some of the other
    students to ask them what they think about the problem.
  \end{enumerate}
\end{notes}


\section{Sports day: Continued}

We meet again our
\href{https://www.icts.res.in/sites/default/files/seminar\%20doc\%20files/MCI_41.pdf\#page=1}{old
  friends}, Arun, Biron and Kiron (ABK).  Last time, we found them considering
the problem of scheduling some selection of $n$ games, where the $i$th game
(where $1 \leq i \leq n$) has exactly $c_i$ conflicts.  They were finally able
to find a way to schedule at least
\begin{equation}
  \sum_{{i=1}}^n \frac{1}{c_i + 1} \label{eq:promise}
\end{equation}
non-conflicting games in such a situation.  To do this, they found helpful the
notion of \emph{ordering} games and then looking for \emph{peaking games} in
those orderings.  To recall, an \emph{ordering} is just a list of games
arranged in order.  Given an ordering $O$, we say that the game $g$ is
\emph{peaking} in $O$ if it has a higher rank in $O$ than all the games it
conflicts with.  For an ordering $O$ and game $g$, we defined $\peak{O, g} = 1$
if $g$ is peaking in $O$ and $\peak{O, g} = 0$ otherwise.  We denote the total
number of games that are peaking in $O$ as $\peak{O}$.  Thus, for every ordering
$O$,
\begin{displaymath}
  \peak{O} = \sum_{{i=1}}^n\peak{O, g_i}.
\end{displaymath}
We also defined $\mathrm{AvgPeak}$ to be the average number of peaking games in
a ordering.  Thus,
\begin{displaymath}
  \mathrm{AvgPeak} = \frac{1}{n!}\sum_O\peak{O},
\end{displaymath}
where the sum is over all possible orderings $O$.  Similarly, we defined
$\mathrm{AvgPeak}(g)$ to be the \emph{fraction} of orderings in which the game
$g$ is peaking.  We then proved the following.
\begin{itemize}
\item For any game $g_{i}$,
  \begin{equation}
    \mathrm{AvgPeak}(g_{i}) = \frac{1}{n!}\sum_O\peak{O, g_i} = \frac{1}{c_{i} + 1},
  \end{equation}
  where the sum is over all orderings $O$.
\item We then saw that
  \begin{displaymath}
   \mathrm{AvgPeak} = \sum_{i=1}^n\mathrm{AvgPeak}(g_i)
 \end{displaymath}
 and that there must exist an ordering $Q$ for which
  \begin{displaymath}
    \peak{Q} \geq \mathrm{AvgPeak}.
  \end{displaymath}
\item Putting these together, we obtained a proof of there being a
  non-conflicting set of games of size at least that given in \cref{eq:promise}.
  We even found that the ordering which arranges games in increasing order of
  number of conflicts always has as many peaks as in \cref{eq:promise}.
  \begin{flushright}
    \emph{[End of recap.]}
  \end{flushright}

  Now, ABK are wondering when is it that the ``best'' schedule is given by the
  set of peaking games in some ordering, and also when is it \emph{not} possible
  to do better than \cref{eq:promise}.

  \begin{enumerate}
  \item Can you construct a list $\mathcal{B}$ of games for which the ``best''
    non-conflicting schedule is \emph{not} the set of peaking games in
    \emph{any} ordering of the games in $\mathcal{B}$?
    \begin{notes}
      This is not possible: just take the best solution, and take an ordering
      starting with these games.
    \end{notes}
  \item Suppose $\mathcal{L}$ is a list of games for which the number of games
    in the ``best'' schedule is \emph{not} larger than what is promised by
    \cref{eq:promise}.  The questions in this part are all in the context of the
    list $\mathcal{L}$.
    %
    \begin{enumerate}
    \item Show that in this case, \emph{every} ordering must have the same
      number of peaking games.
    \item Show that in this case, if game $g$ conflicts with games $h_1$ and
      $h_2$, then the games $h_1$ and $h_2$ must also conflict with each other.
      \begin{notes}
        Take an ordering $Q$ starting with $g, h_1, h_2$ and an ordering $Q'$
        starting with $h_1, h_2, g$, but which is otherwise identical to $Q$. If
        $h_{1}$ and $h_2$ do not conflict then $Q'$ has one more peak (both
        $h_1$ and $h_2$) than $Q$ (only $u$).  This contradicts the above claim
        that all orderings have the same number of peaks.
      \end{notes}
    \item Show therefore that, in this case, if we define $\conf{g}$ to be the
      set of games conflicting with game $g$, then for every game $h$ in
      $\conf{g}$, and every game $k$ different from $g$ and $h$, $k$ is in
      $\conf{g}$ if and only if $k$ is in $\conf{h}$. If game $k_1$ is in the
      set $\conf{g} \cup \inb{g}$ and the game $k_2$ is \emph{not} in the set
      $\conf{g} \cup \inb{g}$, can the games $k_1$ and $k_2$ conflict?
    \item What is the total number of (unordered) conflicting pairs of games
      among the games in $\conf{g} \cup \inb{g}$?
    \end{enumerate}
  \item Suppose now that $\mathcal{L}$ is a list of games about which we only
    know the following facts.  Below $q \geq 3$ is a positive integer.
    \begin{itemize}
    \item There are $qn$ games in $\mathcal{L}$, where $n$ is a positive
      integer.
    \item The number of (unordered) conflicting pairs of games in $\mathcal{L}$
      is $nq(q-1)/2$.
    \end{itemize}
    What is the minimum possible value of the quantity in \cref{eq:promise} for
    such a list?  Can you construct an example of such an $\mathcal{L}$ where
    this minimum value is achieved?
    \begin{notes}
      Must be a disjoint collection of $q$-cliques.
    \end{notes}
  \item Suppose now that $\mathcal{H}$ is \emph{some} list with $qn$ games,
    $nq(q-1)/2$ unordered conflicting pairs in which the number of games in the
    ``best'' schedule is equal to the minimum value in the previous part.  What
    must $\mathcal{H}$ look like?
      \begin{notes}
        Use the previous problem to show that it must look like a disjoint union
        of $q$ cliques.
      \end{notes}
  \end{enumerate}
\end{itemize}


\section{Detecting problems}
ABK are now done with the sports day and have a new problem to tackle. They
found in a library some very old floppy disks on which are saved some of their
favourite story books.  The books are stored in a \emph{binary} format: each
character (letter, digit, punctuation mark, space, \dots) is converted into a
unique sequence of $0$s and
$1$s, and these are then stored (in the same sequence as the sequence of
chracters in the book) on the floppy disks (each such $0$ and
$1$ is referred to as a \emph{bit}).  However, Arun and Barun are worried that
over time, the disks might get subtly corrupted, without their knowledge!  They
want to come up with methods to quickly detect at least some kinds of
corruptions.

\begin{enumerate}
\item ABK believe that the typical corruption would take the form of either
  exactly one of the $0$s flipping to a $1$, or exactly one of the $1$s flipping
  to a $0$.  Can you devise a method for creating a ``summary'' of the book,
  hopefully much smaller than the size of the book itself, and hopefully not too
  hard to calculate given the book, such that the ``summary'' will differ for
  two books that differ exactly by one of the ``bits'' being flipped?
  \begin{notes}
    Simple parity works for this.
  \end{notes}
\item ABK now worry about more subtle corruptions: perhaps two adjacent bits can
  get swapped!  (That is, a pattern of $01$ might get transposed to $10$).  Can
  you now think of a method for creating a ``summary'', just like in the
  previous part, such that the summary would be different for any two books that
  differ exactly by one such swap of adjacent bits?
  \begin{notes}
    Parity does not work anymore.  One solution is to weigh the parity by the
    location of the bit, and store the sum.  If the book has $k$ bits, this
    amounts to storing a number no larger than $k^2$.  We should ask them to
    think about if such a number can be considered a short summary.
  \end{notes}
\end{enumerate}




\end{document}
