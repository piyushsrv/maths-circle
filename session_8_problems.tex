\documentclass[letterpaper,11pt,reqno]{article}
\input{macros}
\newtoggle{notest}
\togglefalse{notest}

\iftoggle{notest}{%
    \NewEnviron{notes}{\color{blue} \BODY} %
  }{%
    \NewEnviron{notes}{}
  }
\usetikzlibrary{calc}
\usepackage{gensymb,tfrupee}
\usepackage[yyyymmdd,hhmmss]{datetime}
\usepackage{float} % for enforcing figure placement
%\usepackage{soul}

\newcommand{\oldfactorial}[1]{%
\tikz[baseline]{\node[anchor=base,inner sep=0.3ex](mynode){\ensuremath{#1}};\draw(mynode.north west)--(mynode.south west)--(mynode.south east);\path[use as bounding box]($(mynode.south west)+(-0.3ex,-0.3ex)$)rectangle($(mynode.north east)+(0.3ex,0.3ex)$);}
}

\newcommand{\peak}[1]{\ensuremath{\mathrm{peak}\inp{#1}}}

\title{Maths Circle India}
\author{TIFR-STCS Maths Circle Team}
\date{Session 8: August 25, 2023}%\\Compiled at \currenttime, \today}

\begin{document}
\maketitle


\begin{notes}
  \paragraph{General notes}
  \begin{enumerate}
  \item Some of the students are much faster than the others.  So, to give
    everyone a fair chance, it might help to sometime call some of the other
    students to ask them what they think about the problem.
  \end{enumerate}
\end{notes}


\section{Sports day: A promise}

A year has passed and Arun, Barun and Kiron (``ABK'') have again been asked to
schedule the sports day.  The situation is
\href{https://www.icts.res.in/sites/default/files/seminar\%20doc\%20files/MCI_41.pdf\#page=1}{exactly
  the same as before}, but this time they are wondering if they can schedule
more games by exploiting the fact that while some games might have a lot of
conflicts, other have very few.  More precisely, they are wondering what is the
best they can do if there are $n$ games, where the $i$th game (where
$1 \leq i \leq n$) has exactly $c_i$ conflicts.  They read in a comment in a
book that in this case, they should be able to schedule at least
\begin{equation}
  \sum_{{i=1}}^n \frac{1}{c_i + 1} \label{eq:promise}
\end{equation}
games.  But the comment section in the book was too small to either give a
proof, or a method for finding such a set of games.

Suppose first that what the book is claiming is actually correct.  Can you come
up with lists where what the book is promising is much better than what we did
last time?  In particular, can you find a list where what is promised above is
at least twice as good as what we had last time?  At least 100 times as good?
(Last time, we could show that if $d_i \leq D$ for every $i$, then we can
schedule at least $n/(D+1)$ games.)


\begin{notes}
  A simple example here is the set of star graphs.
\end{notes}

In the rest of this exploration, we will try to prove what the book claimed.
But before that, let us take a detour and try to analyse ``randomly'' ordered
lists.


\section{``Randomly'' ordered lists}
\label{sec:rand-order-lists}

\begin{notes}
  For this problem, we should \emph{not} mention terms like ``probability'',
  ``expectation'', ``random variable'' or ``linearity of expectation'', as far
  as possible.  But note that some of the students might be familiar with these.
  Unless all the students in the group are familiar with the terms, we should
  just avoid the terms (otherwise they may be needlessly thrown off).
\end{notes}

\begin{enumerate}
\item Suppose that there are $n$ games in ABK's list.  Show that there are a
  total of \[n \times (n-1) \times (n-2) \times \dots \times 2 \times 1\]
  possible orderings of the list.  This number is usually written as $n!$ (and
  sometimes as $\oldfactorial{n}$, especially in older books), and called ``$n$
  factorial''.
\item Suppose that one of these games is \emph{\={a}bol t\={a}bol}.  What is the
  number of orderings in which \emph{\={a}bol t\={a}bol} is ranked first? What
  is the \emph{fraction} of orderings in which \emph{\={a}bol t\={a}bol} is
  ranked first?
\item Can you derive the value of the fraction above \emph{without} actually
  counting the total number of orderings like we did above? (Hint: Suppose we
  fix any \emph{arbitrary} ordering of the other games.  What are the possible
  ranks of \emph{\={a}bol t\={a}bol}? Is each of these ranks obtained in
  a \emph{different} ordering?)

\item Now suppose that we only care about the \emph{relative} rank of
  \emph{\={a}bol t\={a}bol} and \emph{t\={a}bol \={a}bol}.  That is, we only
  care about which one of these two ranks ahead of the other, without worrying
  about the ranks of the other games in the list.  Now, what is the fraction of
  lists in which \emph{\={a}bol t\={a}bol} ranks ahead of \emph{t\={a}bol
    \={a}bol}?

\item Now suppose that we only care about the \emph{relative} rank of
  \emph{\={a}bol t\={a}bol}, \emph{t\={a}bol \={a}bol} and \emph{\^ony\^o
    khel\={a}}.  That is, we only care about which one of these three games
  ranks ahead of the other two, without worrying about the ranks of the other
  games in the list.  Now, what is the fraction of lists in which \emph{\={a}bol
    t\={a}bol} ranks ahead of the other two?

\item In general, suppose that we only care about the relative rank of some $D$
  games $g_{1}, g_2, g_3, \dots, g_D$, irrespective of the ordering of the other
  $n - D$ games (of course, $D \leq n$).  What is the fraction of orderings in
  which $g_1$ ranks ahead of $g_2, g_3, \dots, g_D$?

\item Fix a game $g_i$, which conflicts with $c_i$ other games.  What is the
  fraction of orderings in which $g_i$ ranks above all the games it conflicts
  with?

\end{enumerate}

\paragraph{A linguistic interlude} It is useful to translate the above results
into a slightly different language.  We assign a \emph{weight}
$w(P) = \frac{1}{n!}$ to each ordering $P$.  Note that the weights are non-zero
and sum up to $1$ (such weights are called ``probabilities'').  The fraction of
orderings in which, for example, $g_1$ ranks above $g_2, g_3, \dots, g_D$ is
then just the total weight of all orderings in which the ``event'' that ``$g_1$
ranks above $g_2, g_3, \dots, g_D$'' occurs.  This fraction is then also called
``the \emph{probability} that $g_1$ ranks above $g_2, g_3, \dots, g_D$'' (or,
with more verbosity, ``the \emph{probability} that the \emph{event} that $g_1$
ranks above $g_2, g_3, \dots, g_D$ occurs'').

\section{Peaking games}


Now, given an ordering $O$, we say that the game $g$ is \emph{peaking} in $O$ if
it has a higher rank in $O$ than all the games it conflicts with.  For an
ordering $O$ and game $g$, we define $\peak{O, g} = 1$ if $g$ is peaking in $O$
and $\peak{O, g} = 0$ otherwise.  We denote the total number of games that are
peaking in $O$ as $\peak{O}$.  Thus, for every ordering $O$,
\begin{displaymath}
  \peak{O} = \sum_{{i=1}}^n\peak{O, g_i}.
\end{displaymath}
Let us define $\mathrm{AvgPeak}$ to be the average number of peaking games in a
ordering, where the average is taken with respect to the above weights.  Thus,
\begin{displaymath}
  \mathrm{AvgPeak} = \sum_Ow(O)\peak{O},
\end{displaymath}
where the sum is over all possible orderings $O$.

Similarly, let us define $\mathrm{AvgPeak}(g)$ to be the \emph{fraction} of
orderings in which the game $g$ is peaking.

\begin{enumerate}
\item Fix any ordering $O$, and let $g$ and $h$ be distinct games that are both
  peaking in $O$.  Can $g$ and $h$ have a conflict?
\item Show that for any game $g$,
  \begin{displaymath}
    \mathrm{AvgPeak}(g) = \sum_Ow(O)\peak{O, g},
  \end{displaymath}
  where the sum is over all orderings $O$.  What is the value of
  $\mathrm{AvgPeak}(g)$ in terms of the number of games with which $g$
  conflicts?  (Hint: You already computed this above.)
\item Show therefore that
  \begin{displaymath}
   \mathrm{AvgPeak} = \sum_{i=1}^n\mathrm{AvgPeak}(g_i).
 \end{displaymath}
\item Can you argue that there must exist an ordering $Q$ for which
  \begin{displaymath}
    \peak{Q} \geq \mathrm{AvgPeak}?
  \end{displaymath}
\item Can you now prove the claim that ABK found in the book?
\end{enumerate}


\section{Existence is not enough}
ABK are, of course, quite happy that they have finally proved the claim in the
book.\footnote{
  \begin{quote}
    \emph{Don't just read it; fight it! Ask your own questions, look for your own
      examples, discover your own proofs.}
    \begin{flushright}
      -- \href{https://mathshistory.st-andrews.ac.uk/Biographies/Halmos/quotations/}{Paul Halmos}
    \end{flushright}
  \end{quote}
} Unfortunately, however, it doesn't help with their job to just \emph{know}
that such a set of game \emph{exists}.  They actually need to \emph{find} a
large set of non-conflicting games and give that list to the school, so that the
school knows which games to schedule.

Can you find a reasonably ``fast'' procedure that will actually find a set of
non-conflicting games that is at least as large as what was promised in
\cref{eq:promise} above?


\begin{notes}
  This might be a bit tricky.  The greedy algorithm works: scan vertices in
  order of increasing degree, picking a vertex whenever it does not conflict
  with those already picked.  The reason is that when you pick the lowest degree
  vertex, and then look at 1 (for the already picked lowest degree vertex) + the
  expectation value for the graph obtained by removing the lowest degree vertex
  and its neighbors, the sum term by term dominates the expectation for the
  original graph (just expand 1 as $(d_{\min}+1)\times\frac{1}{d_{\min}+1}$).

  As stated, this only proves that one has to pick the lowest degree vertex in
  the current, reduced graph.  But a simple comparison of terms in the
  expectation shows that just using the degrees in the original graph will also
  work.

  For this problem, we should only give them hints, and a lot of time to them
  for them to solve it.  It is probably best \emph{not} to give out the full
  solution in the class: it is good to leave them with something to think about
  after the session.
\end{notes}


% \section{Is this all?}
% Having finally proved the claim in the book, ABK now decide to read
% further.\footnote{
%   \begin{quote}
%     \emph{Don't just read it; fight it! Ask your own questions, look for your own
%       examples, discover your own proofs.}
%     \begin{flushright}
%       -- Paul Halmos
%     \end{flushright}
%   \end{quote}
% }
% They find that the book makes another claim.



\end{document}
